<header class="header-products header-piping content-row-full">
  <div class="col-sm-12">
    <h1><?php // Get page title
        echo get_the_title(); ?> </h1>
  </div>


</header>
<nav class="breadcrumbs content-row">
  <div class="col-sm-12">
    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) {
      dimox_breadcrumbs();
    } ?>
  </div>
</nav>

<?php

  $custom_terms = get_terms( 'section' );

  foreach ( $custom_terms as $custom_term ) {

    wp_reset_query();
    $args = array(
      'post_type'   => 'pmo_products',
      'numberposts' => 4,
      'tax_query'   => array(
        array(
          'taxonomy' => 'category',
          'field'    => 'slug',
          'terms'    => array( 'cat-piping' ),
        ),
        array(
          'taxonomy' => 'section',
          'field'    => 'slug',
          'terms'    => $custom_term->slug,
        ),
      ),
    );

    $loop = new WP_Query( $args );
    if ( $loop->have_posts() ) {
      $count = 0;
      echo '<div class="prod-row content-row">';

      echo '<div class="prod-section col-xs-12">';
      echo '<h3>' . $custom_term->name . '</h3>';
      echo '</div>';
      while ( $loop->have_posts() ) : $loop->the_post();


        get_template_part( 'templates/prod-item' );
        $count ++;

      endwhile;
      echo '</div>';


    }


  }
  wp_reset_postdata();

?>

<?php get_template_part( 'templates/prod-disclaimer' );?>

