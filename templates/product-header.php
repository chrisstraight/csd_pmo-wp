<header class="hero-home content-row">
  <div class="col-sm-12">
    <h1><?php // Get page title
        echo get_the_title(); ?> </h1>
  </div>


</header>
<nav class="breadcrumbs content-row">
  <div class="col-sm-12">
    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) {
      dimox_breadcrumbs();
    } ?>
  </div>

</nav>

