<?php

  $path_dl
    = ( home_url( '/' ) )."wp-content/uploads/prod_dls/";
  $path_img
    = ( home_url( '/' ) )."wp-content/uploads/prod_img/";

  $path_tn_img
    = ( home_url( '/' ) )."wp-content/uploads/prod_img/thumbs/";
?>
<div id="prod-<?php the_field( 'product_id' ); ?>" class="prod-container col-md-3 col-sm-6">
  <?php if ( get_field( 'product_id' ) ): ?>
  <div class="prod-item">
    <div class="prod-title">
      <ul class="title-block">
        <li>
          <?php // Get terms for post

            $args          = array(
              'orderby' => 'name',
              'order'   => 'DESC',
              'fields'  => 'slug'
            );
            $product_terms = wp_get_object_terms( $post->ID, 'product_names' );
            if ( ! empty( $product_terms ) ) {
              if ( ! is_wp_error( $product_terms ) ) {
                foreach ( $product_terms as $term ) {
                  echo esc_html( $term->name ) . ': ';
                }
              }
            }
          ?>
        </li>
        <li>
          <?php echo get_the_title(); ?>
        </li>
      </ul>
    </div>
      <div class="prod-img">
        <div class="prod-modal-icon fa-search fa fa-2x" data-toggle="modal"
             data-target="#<?php the_field( 'product_id' ); ?>">
        </div>
        <img
          src="<?php echo $path_tn_img, the_field( 'product_id' ); ?>_tn.jpg"
          data-toggle="modal"
          data-target="#<?php the_field( 'product_id' ); ?>"/>
      </div>
      <div class="prod-dls">
        <a
          href="<?php echo $path_dl, the_field( 'product_id' ); ?>.pdf"
          target="_blank">
          <div class="prod-dl col-xs-3 prod-dl-pdf">
          </div>
        </a>
        <a
          href="<?php echo $path_dl, the_field( 'product_id' ); ?>.dwg"
          target="_blank">
          <div class="prod-dl col-xs-3 prod-dl-dwg">
          </div>
        </a>
        <a
          href="<?php echo $path_dl, the_field( 'product_id' ); ?>.hpj"
          target="_blank">
          <div class="prod-dl col-xs-3 prod-dl-hpj">
          </div>
        </a>
        <a
          href="<?php echo $path_dl, the_field( 'product_id' ); ?>_Calc-Report.pdf"
          target="_blank">
          <div class="prod-dl col-xs-3 prod-dl-pkg">
          </div>
        </a>
      </div>
    <?php endif; ?>
  </div>
</div>
<!-- Modal -->
<div class="modal" id="<?php the_field( 'product_id' ); ?>" tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel">
  <button type="button" class="close" data-dismiss="modal"
          aria-label="Close"><span
      aria-hidden="true"><?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-close.svg" ); ?></span>
  </button>
  <div class="modal-body">
    <h3 class="modal-header">
      <?php echo get_the_title(); ?>
    </h3>
    <img
      src="<?php echo $path_img, the_field( 'product_id' ); ?>.jpg"/>
  </div>
</div>

