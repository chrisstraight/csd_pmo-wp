<footer class="nav-footer">
  <div class="content-row">
    <div class="col-md-2 col-sm-3">
      <ul>
        <li class="footer-logo">
          <a href="<?= esc_url( home_url( '/' ) ); ?>"></a>
        </li>
        <li>
          <ul class="footer-social">
            <li class="social-link">
              <a href="https://www.facebook.com/HiltiNorthAmerica" target="_blank"><i
                  class="fa-facebook-square fa fa-2x"></i></a>
            </li>
            <li class="social-link">
              <a href="https://www.youtube.com/user/HiltiNorthAmerica" target="_blank"><i
                  class="fa-youtube-square fa fa-2x"></i></a>
            </li>
            <li class="social-link">
              <a href="https://twitter.com/HiltiNAmerica?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"><i
                  class="fa-twitter-square fa fa-2x"></i></a>
            </li>
            <li class="social-link">
              <a href="https://instagram.com/hiltinorthamerica" target="_blank">
                <i class="fa-instagram fa fa-2x"></i>
              </a>
            </li>
          </ul>
        </li>
        <li></li>
      </ul>
    </div>
    <div class="col-md-10 col-sm-9">
      <div class="row">
        <div class="col-sm-12">
          <div class="col-md-3 col-sm-4">
            <ul>
              <li>
                <a href="<?php echo get_page_link( 7 ); ?>">Products</a>
              </li>
              <li>
                <a href="<?php echo get_page_link( 29 ); ?>">Piping Supports</a>
              </li>
              <li>
                <a href="<?php echo get_page_link( 28 ); ?>">Cable Tray
                  Supports
                </a>
              </li>
              <li>
                <a href="<?php echo get_page_link( 27 ); ?>">Platforms</a>
              </li>
              <li>
                <a href="<?php echo get_page_link( 10 ); ?>">Electrical and
                  Instrumentation Stands
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-4">
            <ul>
              <li>
                <a href="<?php echo get_page_link( 12 ); ?>">Services</a>
              </li>
              <li>
                <a href="<?php echo get_page_link( 18 ); ?>" target="_blank">Request Proposal</a>
              </li>
            </ul>
            <ul>
              <li class="footer-header">Resource Links:</li>
              <li>
                <a href="https://www.us.hilti.com/Engineering/design-software"
                   target="_blank">Hilti Design Resoures
                </a>
              </li>
              <li>
                <a href="https://www.us.hilti.com/specifications-details"
                   target="_blank">Hilti Specification Guidlines
                </a>
              </li>
              <li>
                <a href="https://www.us.hilti.com/submittals" target="_blank">
                  Generate Hilti Submittal
                </a>
              </li>
              <li>
                <a href="https://www.us.hilti.com/technical-advice"
                   target="_blank">Hilti Technical Support
                </a>
              </li>
              <li>
                <a
                  href="https://www.us.hilti.com/medias/sys_master/documents/h19/9161585491998/Anchor_Selector_Chart_Brochure_ASSET_DOC_LOC_3392867.pdf"
                  target="_blank">Anchor Selector Guide
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-4">
            <ul>
              <li>
                <a href="<?php echo get_page_link( 14 ); ?>">Software</a>
              </li>
              <li>
                <a href="http://www.us.hilti.com/profis" target="_blank">Profis
                </a>
              </li>
              <li>
                <a href="https://www.us.hilti.com/bim" target="_blank">BIM/CAD
                  Library
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-4">
            <ul>
              <li>
                <a href="<?php echo get_page_link( 318 ); ?>">FAQs</a>
              </li>
              <li>
                <a href="<?php echo get_page_link( 18 ); ?>">Contact</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
