<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
<!--    <!--        <div class="row">-->-->
<!--    <!--      <div class="navbar-logo col-xs-12">--><!--            <a href="-->-->
<!--    --><?// // //= esc_url( home_url( '/' ) ); ?><!--<!--"><img-->-->
<!--    <!--                src="http://fpoimg.com/300x250"></a>-->-->
<!--    <!--      </div>-->-->
  </div>
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed"
            data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only"><?= __( 'Toggle navigation', 'sage' ); ?></span>
      <span class="icon-bar"></span> <span class="icon-bar"></span> <span
        class="icon-bar"></span>
    </button>
  </div>
  <nav class="collapse navbar-collapse" role="navigation">
    <?php

      if ( has_nav_menu( 'primary_navigation' ) ) :
        wp_nav_menu( [
          'theme_location' => 'primary_navigation',
          'menu_class'     => 'nav navbar-nav',
          'walker'         => new wp_bootstrap_navwalker()
        ] );
      endif;
    ?>
  </nav>
  </div>
</header>
