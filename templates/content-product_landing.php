<header class="header-sub-text content-row">
  <div class="col-sm-12">
    <h1><?php // Get page title
        echo get_the_title( $ID ); ?> </h1>
  </div>
</header>
<section>
  <div class="category-nav content-row">
    <div class="col-sm-6 category-nav-block nav-block-1 bg-img">
      <a href="<?php echo get_permalink(29); ?>" class="nav-block-link">
      <h2>Piping Supports</h2>
      <p>Donec ullamcorper nulla non metus auctor fringilla.</p>
      </a>
    </div>
    <div class="col-sm-6 category-nav-block nav-block-2 bg-img">
      <a href="<?php echo get_permalink(28); ?>" class="nav-block-link">
        <h2>Cable Tray Supports</h2>
        <p>Donec ullamcorper nulla non metus auctor fringilla.</p>
      </a>
    </div>
  </div>
  <div class="category-nav content-row">
    <div class="col-sm-6 category-nav-block nav-block-3 bg-img">
      <a href="<?php echo get_permalink(27); ?>" class="nav-block-link">
        <h2>Platforms</h2>
        <p>Donec ullamcorper nulla non metus auctor fringilla.</p>
      </a>
    </div>
    <div class="col-sm-6 category-nav-block nav-block-4 bg-img">
      <a href="<?php echo get_permalink(10); ?>" class="nav-block-link">
        <h2>Electrical & Instrumentation Stands
        </h2>
        <p>Donec ullamcorper nulla non metus auctor fringilla.</p>
      </a>
    </div>
  </div>
</section>