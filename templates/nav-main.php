<div class="nav-logo">
  <a href="<?= esc_url( home_url( '/' ) ); ?>"><img
      src="<?php bloginfo( 'template_url' ); ?>/dist/images/logo.svg"
      alt="Hilti logo"></a>
</div>
<nav class="nav-menu">
  <?php
    if ( has_nav_menu( 'primary_navigation' ) ) :
      wp_nav_menu( [
        'theme_location' => 'primary_navigation',
        'walker'         => new wp_bootstrap_navwalker()
      ] );
    endif;
  ?>
</nav>