<header class="header-home header-image header-text content-row-full">
  <div class="col-sm-12">
    <div class="header-home-text">
      <h1>Hilti Pre-Engineered Solutions.</h1>
      <p>Helping you deliver on time and under budget on your most complex
        projects.
      </p>
    </div>
    <div class="header-cta">
      <a href="#">
        <div class="header-cta-icon" data-toggle="modal"
             data-target="#header-video">
          <img
            src="<?php bloginfo( 'template_url' ); ?>/dist/images/icons/icon-play.png"
            alt="product icon">
        </div>

        <p class="header-cta-text" data-toggle="modal"
           data-target="#header-video">
            See our project management office in action
          </p>
      </a>
  </div>
</header>
<section id="home-intro">
  <div class="nav-block-container content-row-full">
    <a href="<?php echo get_page_link( 7 ); ?>"
       class="col-sm-4 nav-block bg-primary">
      <div class="block-icon">
        <?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-products.svg"); ?>
      </div>
      <p>Products</p>
      <p>Pre-engineered modular support and strut systems.</p>
      <div class="arrow-icon">
        <?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-arrow.svg" ); ?>
      </div>
    </a>
    <a href="<?php echo get_page_link( 12 ); ?>"
       class="col-sm-4 nav-block bg-darker">
      <div class="block-icon">
        <?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-services.svg" ); ?>
      </div>
      <p>Services</p>
      <p>Custom engineering, bidding, and fullfillment services.</p>
      <div class="arrow-icon">
        <?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-arrow.svg" ); ?>
      </div>
    </a>
    <a href="<?php echo get_page_link( 14 ); ?>"
       class="col-sm-4 nav-block bg-primary">
      <div class="block-icon">
        <?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-software.svg" ); ?>
      </div>
      <p>Software</p>
      <p>Get more done faster with our custom design software and tools.
      <div class="arrow-icon">
        <?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-arrow.svg" ); ?>
      </div>
    </a>
  </div>
</section>
<section id="home-compare">
  <div class="content-compare bg-light content-row-full">
    <div class="content-left col-sm-4">
      <h3>With Hilti Solutions lower your total installed cost utilizing modular
        flexible installation systems
      </h3>
    </div>
    <div class="content-right col-sm-8">
      <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/photos/graph-home.png"
           alt="comparison chart">
    </div>
  </div>
</section>
<section id="home-testimonial-1">
  <div class="content-testimonial bg-primary content-row-full">
    <div class="col-sm-10 col-sm-push-1">
      <p class="testimonial-quote">“I want to thank the HILTI Team for the
        support provided to Gaumer during the Wheat Stone project. HILTI’s MQ
        Strut System really came through for us, and the professional
        calculations and documentation really helped when it came to producing
        the final project databook. I look forward to working with HILTI on
        future projects, and will continue to turn to your organization for new
        and innovative solutions to everyday problems.”
      </p>
      <p class="testimonial-byline">
        – Matt Buss, Project Specialist at Gaumer Process
      </p>
    </div>
  </div>
</section>
<section id="home-video">
  <div class="content-video bg-darker content-row-full">
    <div class="col-sm-4 video-cta">
      <h3>Learn more about HIlti modular support systems</h3>
    </div>
    <div class="col-sm-8 video-container">
      <div class="video-frame"></div>
      <iframe
              src="https://www.youtube.com/embed/zUv9rg7itn0?wmode=transparent&amp;rel=0&amp;controls=1&amp;showinfo=0"
              frameborder="0" allowfullscreen></iframe>
    </div>
  </div>
</section>
<section id="home-table">
  <div class="content-table bg-light content-row-full">
    <div class="col-sm-8 col-sm-push-2">
      <h3>Hilti Modular Support Systems vs. Welding</h3>
      <div class="table-container">
        <div class="table-row">
          <div class="table-cell">
            <p
            </p>
          </div>
          <div class="table-cell">
            <p>Hilti Modular Support Systems</p>
          </div>
          <div class="table-cell">
            <p>Welding</p>
          </div>
        </div>
        <div class="table-row">
          <div class="table-cell">
            <p>Pre-Assembly</p>
          </div>
          <div class="table-cell">
            <p>82 minutes</p>
          </div>
          <div class="table-cell">
            <p>n/a</p>
          </div>
        </div>
        <div class="table-row">
          <div class="table-cell">
            <p>Installation</p>
          </div>
          <div class="table-cell">
            <p>66 minutes</p>
          </div>
          <div class="table-cell">
            <p>511 minutes</p>
          </div>
        </div>
        <div class="table-row">
          <div class="table-cell">
            <p>Relocation</p>
          </div>
          <div class="table-cell">
            <p>6.6 minutes</p>
          </div>
          <div class="table-cell">
            <p>51 minutes</p>
          </div>
        </div>
        <div class="table-row">
          <div class="table-cell">
            <p>Manpower</p>
          </div>
          <div class="table-cell">
            <p>$131</p>
          </div>
          <div class="table-cell">
            <p>$530</p>
          </div>
        </div>
        <div class="table-row">
          <div class="table-cell">
            <p>Materials</p>
          </div>
          <div class="table-cell">
            <p>$244</p>
          </div>
          <div class="table-cell">
            <p>$325</p>
          </div>
        </div>
        <div class="table-row">
          <div class="table-cell">
            <p>Total Installed Cost</p>
          </div>
          <div class="table-cell">
            <p>$375</p>
          </div>
          <div class="table-cell">
            <p>$855</p>
          </div>
        </div>
      </div>
      <p class="footnote">Actual results from a simple offshore oil structure as
        reported by an independent test, research and consultancy non-profit
        organization.
      </p>
    </div>
</section>
<section id="home-carousel">
  <div class="content-carousel bg-darker content-row-full">
    <div class="col-sm-12 carousel-container">
      <div id="carousel-home" class="carousel slide"
           data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active row">
            <div class="col-sm-4 text-container">
              <h3>Cable tray supports for large energy facility.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-1.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Cable tray supports for oil sands project.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-2.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Cable tray supports on a natural gas project in high-cost
                labor market.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-3.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Electrical and instrumentation supports on offshore platform.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-4.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Electrical supports on a combined cycle gas turbine project.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-5.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Equipment racking for nuclear uprate and standardization
                project.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-6.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Equipment supports and access platforms for copper mine.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-7.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Integrated Floor System for offshore LNG project.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-8.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Overhead grid with hot:cold aisle structure for data center.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-9.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Panel stand supports, kitted with anchors for large utility
                customer.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-10.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Piping supports on wastewater facility in the Northwest.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-11.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Raised floor application for large data center.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-12.jpeg"
                alt="...">
            </div>
          </div>
          <div class="item row">
            <div class="col-sm-4 text-container">
              <h3>Small bore pipe supports on a combined cycle gas turbine
                project.
              </h3>
            </div>
            <div class="col-sm-8 image-container">
              <div class="carousel-frame"></div>
              <img
                src="<?php echo( home_url( '/' ) ); ?>wp-content/uploads/carousel/pmo-ss-13.jpeg"
                alt="...">
            </div>
          </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control"
           href="#carousel-home" role="button"
           data-slide="prev">
          <div class="control-button-left"><span
              class="glyphicon glyphicon-menu-left"
              aria-hidden="true"></span>
          </div>
        </a>
        <a class="right carousel-control"
           href="#carousel-home" role="button"
           data-slide="next">
          <div class="control-button-right"><span
              class="glyphicon glyphicon-menu-right"
              aria-hidden="true"></span></div>
        </a>
      </div>
    </div>
  </div>
</section>
<section id="home-testimonial-2">
  <div class="content-testimonial bg-primary content-row-full">
    <div class="col-sm-10 col-sm-push-1">
      <p class="testimonial-quote">“I would like to extend our appreciation for
        the service and expertise of Hilti regarding structural support systems
        and helping us create new ways to utilize heavy modular support systems.
        Hilti was instrumental helping us to specify and design the heavy
        support system for temporary electrical switch racks and electrical
        equipment supports. We were able to provide a “non-welded” option to our
        customers and expedite delivery to improve schedule without compromising
        quality. Once again we want to thank Hilti for the support and look
        forward to working with you in the future.”
      </p>
      <p class="testimonial-byline">
        – Raymond T. Shrum, President at Amber L.P.
      </p>
    </div>
  </div>
</section>
<div class="modal" id="header-video" tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
  <button type="button" class="close" data-dismiss="modal"
          aria-label="Close"><span
      aria-hidden="true"><?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-close.svg" ); ?></span>
  </button>
  <iframe id="video" width="1280" height="720"
          src="https://www.youtube.com/embed/3bCDz01wuJo?rel=0&amp;controls=1&amp;showinfo=0&amp;enablejsapi=1"
          frameborder="0" allowfullscreen></iframe>
</div>