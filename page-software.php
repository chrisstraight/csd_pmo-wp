<header class="header-sub content-row">
  <div class="col-sm-12">
    <h1>Get more done faster with our custom complimentary design software and
      tools
    </h1>
    <div class="header-cta">
      <a href="#">
        <div class="header-cta-icon" data-toggle="modal"
             data-target="#header-video"/>
    <img
      src="../wp-content/themes/pmo/dist/images/icons/icon-play.png"
      alt="product icon">
    </div>

    <p class="header-cta-text" data-toggle="modal"
       data-target="#header-video"> See our 3D Design Software in Action.
    </p>
    </a>
  </div>
</header>
<section>
  <div class="software-nav content-row-full">
    <div class="col-sm-6 software-nav-block">
      <a href="http://www.us.hilti.com/profis" target="_blank"
         class="nav-block-bg nav-block-left">
        <div class="hover">
          <div class="col-sm-10">
            <p>Download and start designing today with Hilti PROFIS software.
            </p>
          </div>
          <div class="col-sm-2">
            <div class="arrow-icon">
              <?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-arrow.svg" ); ?>
            </div>
          </div>
        </div>
        <div class="default">
          <div class="software-detail">
            <h2>Profis</h2>
            <p>Fully-compatible with our comprehensive BIM/CAD object database,
              Hilti PROFIS software delivers fast, accurate solutions to your
              application problems whenever you need them.
            </p>
            <ul>
              <li>PROFIS Anchor</li>
              <li>PROFIS Rebar</li>
              <li>PROFIS Installation</li>
              <li>PROFIS DF Diaphragm</li>
              <li>PROFIS Anchor Channel</li>
              <li>And more</li>
            </ul>
          </div>
        </div>
      </a>
    </div>
    <div class="col-sm-6 software-nav-block">
      <a href="https://www.us.hilti.com/bim" target="_blank"
         class="nav-block-bg nav-block-right">
        <div class="hover">
          <div class="col-sm-10">
            <p>Start browsing Hilti's BIM and CAD Library now.</p>
          </div>
          <div class="col-sm-2">
            <div class="arrow-icon">
              <?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-arrow.svg" ); ?>
            </div>
          </div>
        </div>
        <div class="default">
          <div class="software-detail">
            <h2>BIM/CAD Library</h2>
            <p>An extensive range of Hilti products are available as 2D or 3D
              BIM/CAD objects.
            </p>
          </div>
        </div>
      </a>
    </div>
  </div>
</section>
<div class="modal" id="header-video" tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
  <button type="button" class="close" data-dismiss="modal"
          aria-label="Close"><span
      aria-hidden="true"><?php echo file_get_contents( "wp-content/themes/pmo/dist/images/icons/icon-close.svg" ); ?></span>
  </button>
  <iframe width="1280" height="720"
          src="https://www.youtube.com/embed/G9uiZJOmQ74?rel=0&amp;controls=1&amp;showinfo=0"
          frameborder="0" allowfullscreen></iframe>
</div>