<header class="header-sub header-text content-row">
  <div class="col-sm-12">
    <h1><?php // Get page title
        echo get_the_title(); ?> </h1>
    <p>Customers have come to expect superior products from Hilti. Now you can
      also rely upon the services and solutions from our Project Management Office to
      help you finish on time and under budget.
    </p>
  </div>
</header>
<section>
  <div class="chart-container content-row">
    <div class="chart-section col-sm-3">
      <div class="chart-header">
        <div class="block-icon block-icon-1">
        </div>
        <h4>
          Engineer
        </h4>
      </div>
      <div class="chart-content">
        <ul class="chart-bullets">
          <li>
            <p>Structural design services
            </p>
          </li>
          <li>
            <p>Plant design modeling</p>
            <p>Libraries for the main 3D Plant design software (Aveva PDMS,
              Intergraph SmartPlant)
            </p>
          </li>
          <li>
            <p>Detail and shop drawings</p>
            <p>Library of components for most common AutoCAD systems</p>
          </li>
          <li>
            <p>Professional Engineering Stamps</p>
            <p>Available for all 50 states and Canadian provinces through our
              Hilti Accredited Structural Engineer partnerships
            </p>
          </li>
        </ul>
      </div>
    </div>
    <div class="chart-divider col-md-1">
      <div class="divider-img"></div>
    </div>
    <div class="chart-section col-sm-3">
      <div class="chart-header">
        <div class="block-icon block-icon-2">
        </div>
        <h4>
          Bid / Estimate
        </h4>
      </div>
      <div class="chart-content">
        <ul class="chart-bullets">
          <li>
            <p>Customized project quotations</p>
          </li>
          <li>
            <p>Individualized procurement</p>
          </li>
          <li>
            <p>Budget planning</p>
          </li>
        </ul>
      </div>
    </div>
    <div class="chart-divider col-md-1">
      <div class="divider-img"></div>
    </div>
    <div class="chart-section col-sm-3">
      <div class="chart-header">
        <div class="block-icon block-icon-3">
        </div>
        <h4>
          Fulfillment
        </h4>
      </div>
      <div class="chart-content">
        <ul class="chart-bullets">
          <li>
            <p>Measuring and cutting</p>
          </li>
          <li>
            <p>Custom kitting</p>
          </li>
          <li>
            <p>On-site inventory management</p>
          </li>
          <li>
            <p>Stage deliveries to schedule</p>
          </li>
          <li>
            <p>Package and label by floor</p>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="services-cta content-row">
  <a href="<?php echo get_page_link( 18 ); ?>">
    <div class="cta-container col-sm-12">
      <div class="cta-left">
        Click here to request any of the above services for your project.
      </div>
      <div class="cta-right">
      </div>
    </div>
  </a>
</div>
<div class="resource-links content-row-fluid">
  <ul class="link-row">
    <li class="link-item">
      <a href="https://www.us.hilti.com/Engineering/design-software"
         target="_blank">Hilti Design<br> Resources
      </a>
    </li>
    <li class="link-item">
      <a href="https://www.us.hilti.com/specifications-details"
         target="_blank">Hilti Specification<br> Guidelines
      </a>
    </li>
    <li class="link-item">
      <a href="https://www.us.hilti.com/submittals" target="_blank">Generate
        Hilti<br> Submittal
      </a>
    </li>
    <li class="link-item">
      <a href="https://www.us.hilti.com/technical-advice"
         target="_blank">Hilti Technical<br> Support
      </a>
    </li>
    <li class="link-item">
      <a
        href="https://www.us.hilti.com/medias/sys_master/documents/h19/9161585491998/Anchor_Selector_Chart_Brochure_ASSET_DOC_LOC_3392867.pdf"
        target="_blank">Anchor Selector<br> Guide
      </a>
    </li>
  </ul>
</div>
