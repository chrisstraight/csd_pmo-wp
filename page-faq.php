<header class="header-sub header-text content-row">
  <div class="col-sm-12">
    <h1>Frequently Asked Questions</h1>
  </div>
</header>
<?php

  // check if the repeater field has rows of data
  if ( have_rows( 'faq_group' ) ):
    $i = 0;

    // loop through the rows of data
    while ( have_rows( 'faq_group' ) ) :
      the_row();
//      $i ++;
      ?>
      <div class="faq-group-title content-row-fluid">
        <div class="content-row">
          <div class="col-xs-12">
            <?php the_sub_field( 'faq_section_title' ); ?>
          </div>
        </div>
      </div>
      <div class="content-row">
        <div class="col-xs-12">
          <div class="panel-group" id="accordion<?php echo $i ?>" role="tablist"
               aria-multiselectable="false">
            <?php

              // check if the repeater field has rows of data
              if ( have_rows( 'faq_entry' ) ):
                // loop through the rows of data
                while ( have_rows( 'faq_entry' ) ) :
                  the_row();
                  $i ++;
                  ?>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab"
                         id="id<?php echo $i ?>">
                      <h4 class="panel-title">
                            <?php the_sub_field( 'faq_title' ); ?>
                      </h4>
                    </div>
                    <div class="panel-body">
                      <p><?php the_sub_field( 'faq_content' ); ?></p>
                    </div>
                  </div>
                  <?php

                endwhile;

              else :

              endif;

            ?>
          </div>
        </div>
      </div>
      <?php

    endwhile;

  else :

  endif;

?>
