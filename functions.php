<?php
  /**
   * Sage includes
   * The $sage_includes array determines the code library included in your
   * theme. Add or remove files to the array as needed. Supports child theme
   * overrides. Please note that missing files will produce a fatal error.
   *
   * @link https://github.com/roots/sage/pull/1042
   */
  $sage_includes = [
    'lib/assets.php',  // Scripts and stylesheets
    'lib/extras.php',  // Custom functions
    'lib/setup.php',   // Theme setup
    'lib/titles.php',  // Page titles
    'lib/wrapper.php'  // Theme wrapper class
  ];

  foreach ( $sage_includes as $file ) {
    if ( ! $filepath = locate_template( $file ) ) {
      trigger_error( sprintf( __( 'Error locating %s for inclusion', 'sage' ),
        $file ), E_USER_ERROR );
    }

    require_once $filepath;
  }
  unset( $file, $filepath );

  // Register Custom Navigation Walker
  require_once( 'wp_bootstrap_navwalker.php' );

  // Add suport for ACF options page
  if ( function_exists( 'acf_add_options_page' ) ) {

    acf_add_options_page();

  }

  // Register main menus

  register_nav_menus( array(
    'footer' => __( 'Footer Nav', 'PMO' ),
  ) );


  /*
* Products CPT
*/

  add_action( 'init', 'create_post_type' );
  function create_post_type() {
    register_post_type( 'pmo_products',
      array(
        'labels'      => array(
          'name'               => __( 'Products' ),
          'singular_name'      => __( 'Product' ),
          'add_new'            => __( 'Add Product' ),
          'add_new_item'       => __( 'Add New Product' ),
          'edit'               => __( 'Edit' ),
          'edit_item'          => __( 'Edit Product' ),
          'new_item'           => __( 'New Product' ),
          'view'               => __( 'View Product' ),
          'view_item'          => __( 'View Product' ),
          'search_items'       => __( 'Search Products' ),
          'not_found'          => __( 'No products found' ),
          'not_found_in_trash' => __( 'No products found in Trash' ),
        ),
        'taxonomies'  => array( 'category', 'section', 'product_names' ),
        'public'      => true,
        'has_archive' => true,
      )
    );
  }

  /*
* Custom Taxonomies for Products
*/

// hook into the init action and call create_product_taxonomies when it fires
  add_action( 'init', 'create_product_taxonomies', 0 );

// create two taxonomies, genres and writers for the post type "book"
  function create_product_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
      'name'                       => _x( 'Sections', 'taxonomy general name' ),
      'singular_name'              => _x( 'Section', 'taxonomy singular name' ),
      'search_items'               => __( 'Search Sections' ),
      'all_items'                  => __( 'All Sections' ),
      'parent_item'                => __( 'Parent Section' ),
      'parent_item_colon'          => __( 'Parent Section:' ),
      'edit_item'                  => __( 'Edit Section' ),
      'update_item'                => __( 'Update Section' ),
      'add_new_item'               => __( 'Add New Section' ),
      'new_item_name'              => __( 'New Section Name' ),
      'menu_name'                  => __( 'Section' ),
      'name'                       => _x( 'Sections', 'taxonomy general name' ),
      'singular_name'              => _x( 'Section', 'taxonomy singular name' ),
      'search_items'               => __( 'Search Sections' ),
      'popular_items'              => __( 'Popular Sections' ),
      'all_items'                  => __( 'All Sections' ),
      'parent_item'                => null,
      'parent_item_colon'          => null,
      'edit_item'                  => __( 'Edit Section' ),
      'update_item'                => __( 'Update Section' ),
      'add_new_item'               => __( 'Add New Section' ),
      'new_item_name'              => __( 'New Writer Name' ),
      'separate_items_with_commas' => __( 'Separate section with commas' ),
      'add_or_remove_items'        => __( 'Add or remove sections' ),
      'choose_from_most_used'      => __( 'Choose from the most used sections' ),
      'not_found'                  => __( 'No sections found.' ),
      'menu_name'                  => __( 'Sections' ),
    );

    $args = array(
      'hierarchical'          => false,
      'labels'                => $labels,
      'show_ui'               => true,
      'show_admin_column'     => true,
      'update_count_callback' => '_update_post_term_count',
      'query_var'             => true,
      'rewrite'               => array( 'slug' => 'section' ),
    );

    register_taxonomy( 'section', 'pmo_products', $args );

    $labels = array(
      'name'                       => _x( 'Product Names',
        'taxonomy general name' ),
      'singular_name'              => _x( 'Product Name',
        'taxonomy singular name' ),
      'search_items'               => __( 'Search Product Names' ),
      'popular_items'              => __( 'Popular Product Names' ),
      'all_items'                  => __( 'All Product Name' ),
      'parent_item'                => null,
      'parent_item_colon'          => null,
      'edit_item'                  => __( 'Edit Product Name' ),
      'update_item'                => __( 'Update Product Name' ),
      'add_new_item'               => __( 'Add New Product Name' ),
      'new_item_name'              => __( 'New Product Name Name' ),
      'separate_items_with_commas' => __( 'Separate product names with commas' ),
      'add_or_remove_items'        => __( 'Add or remove product names' ),
      'choose_from_most_used'      => __( 'Choose from the most used product names' ),
      'not_found'                  => __( 'No product names found.' ),
      'menu_name'                  => __( 'Product Names' ),

    );

    $args = array(
      'hierarchical'          => false,
      'labels'                => $labels,
      'show_ui'               => true,
      'show_admin_column'     => true,
      'update_count_callback' => '_update_post_term_count',
      'query_var'             => true,
      'rewrite'               => array( 'slug' => 'product_names' ),
    );

    register_taxonomy( 'product_names', 'pmo_products', $args );

  }


  /*********************
   * re-order left admin menu
   **********************/
//  function reorder_admin_menu( $__return_true ) {
//    return array(
//      'index.php', // Dashboard
//      'edit.php?post_type=pmo_products', // Pages
//      'separator1', // --Space--
//      'edit.php', // Posts
//      'upload.php', // Media
//      'themes.php', // Appearance
//      'users.php', // Users
//      'separator2', // --Space--
//      'plugins.php', // Plugins
//      'tools.php', // Tools
//      'options-general.php', // Settings
//    );
//  }
//
//  add_filter( 'custom_menu_order', 'reorder_admin_menu' );
//  add_filter( 'menu_order', 'reorder_admin_menu' );

  /*
//* Remove top level and sub menu admin menus
//*/
//  function remove_admin_menus() {
//
//    remove_menu_page( 'edit.php' ); // Comments
//    remove_menu_page( 'edit-comments.php' ); // Comments
//  }
//
//  add_action( 'admin_menu', 'remove_admin_menus' );
//
//  // Remove "tags" from pmo_products
//  add_action( 'init', 'pmo_products' );
//
//  function my_register_post_tags() {
//    register_taxonomy( 'post_tag', array( 'pmo_products' ) );
//  }


  /*
* Breadcrumbs
   * http://dimox.net/wordpress-breadcrumbs-without-a-plugin/
*/

  function dimox_breadcrumbs() {
    /* === OPTIONS === */
    $text['home']     = 'Home'; // text for the 'Home' link
    $text['category'] = 'Archive by Category "%s"'; // text for a category page
    $text['search']
                      = 'Search Results for "%s" Query'; // text for a search results page
    $text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
    $text['author']   = 'Articles Posted by %s'; // text for an author page
    $text['404']      = 'Error 404'; // text for the 404 page
    $text['page']     = 'Page %s'; // text 'Page N'
    $text['cpage']    = 'Comment Page %s'; // text 'Comment Page N'
    $wrap_before      = '<div class="breadcrumbs">'; // the opening wrapper tag
    $wrap_after
                      = '</div><!-- .breadcrumbs -->'; // the closing wrapper tag
    $sep              = '›'; // separator between crumbs
    $sep_before       = '<span class="sep">'; // tag before separator
    $sep_after        = '</span>'; // tag after separator
    $show_home_link   = 1; // 1 - show the 'Home' link, 0 - don't show
    $show_on_home
                      = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $show_current     = 1; // 1 - show current page title, 0 - don't show
    $before
                      = '<span class="current">'; // tag before the current crumb
    $after            = '</span>'; // tag after the current crumb
    /* === END OF OPTIONS === */
    global $post;
    $home_link      = home_url( '/' );
    $link_before
                    = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
    $link_after     = '</span>';
    $link_attr      = ' itemprop="url"';
    $link_in_before = '<span itemprop="title">';
    $link_in_after  = '</span>';
    $link           = $link_before . '<a href="%1$s"' . $link_attr . '>'
                      . $link_in_before . '%2$s' . $link_in_after . '</a>'
                      . $link_after;
    $frontpage_id   = get_option( 'page_on_front' );
    $parent_id      = $post->post_parent;
    $sep            = ' ' . $sep_before . $sep . $sep_after . ' ';
    if ( is_home() || is_front_page() ) {
      if ( $show_on_home ) {
        echo $wrap_before . '<a href="' . $home_link . '">' . $text['home']
             . '</a>' . $wrap_after;
      }
    } else {
      echo $wrap_before;
      if ( $show_home_link ) {
        echo sprintf( $link, $home_link, $text['home'] );
      }
      if ( is_category() ) {
        $cat = get_category( get_query_var( 'cat' ), false );
        if ( $cat->parent != 0 ) {
          $cats = get_category_parents( $cat->parent, true, $sep );
          $cats = preg_replace( "#^(.+)$sep$#", "$1", $cats );
          $cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#',
            $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2'
            . $link_in_after . '</a>' . $link_after, $cats );
          if ( $show_home_link ) {
            echo $sep;
          }
          echo $cats;
        }
        if ( get_query_var( 'paged' ) ) {
          $cat = $cat->cat_ID;
          echo $sep . sprintf( $link, get_category_link( $cat ),
              get_cat_name( $cat ) ) . $sep . $before . sprintf( $text['page'],
              get_query_var( 'paged' ) ) . $after;
        } else {
          if ( $show_current ) {
            echo $sep . $before . sprintf( $text['category'],
                single_cat_title( '', false ) ) . $after;
          }
        }
      } elseif ( is_search() ) {
        if ( have_posts() ) {
          if ( $show_home_link && $show_current ) {
            echo $sep;
          }
          if ( $show_current ) {
            echo $before . sprintf( $text['search'], get_search_query() )
                 . $after;
          }
        } else {
          if ( $show_home_link ) {
            echo $sep;
          }
          echo $before . sprintf( $text['search'], get_search_query() )
               . $after;
        }
      } elseif ( is_day() ) {
        if ( $show_home_link ) {
          echo $sep;
        }
        echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ),
            get_the_time( 'Y' ) ) . $sep;
        echo sprintf( $link,
          get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ),
          get_the_time( 'F' ) );
        if ( $show_current ) {
          echo $sep . $before . get_the_time( 'd' ) . $after;
        }
      } elseif ( is_month() ) {
        if ( $show_home_link ) {
          echo $sep;
        }
        echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ),
          get_the_time( 'Y' ) );
        if ( $show_current ) {
          echo $sep . $before . get_the_time( 'F' ) . $after;
        }
      } elseif ( is_year() ) {
        if ( $show_home_link && $show_current ) {
          echo $sep;
        }
        if ( $show_current ) {
          echo $before . get_the_time( 'Y' ) . $after;
        }
      } elseif ( is_single() && ! is_attachment() ) {
        if ( $show_home_link ) {
          echo $sep;
        }
        if ( get_post_type() != 'post' ) {
          $post_type = get_post_type_object( get_post_type() );
          $slug      = $post_type->rewrite;
          printf( $link, $home_link . '/' . $slug['slug'] . '/',
            $post_type->labels->singular_name );
          if ( $show_current ) {
            echo $sep . $before . get_the_title() . $after;
          }
        } else {
          $cat  = get_the_category();
          $cat  = $cat[0];
          $cats = get_category_parents( $cat, true, $sep );
          if ( ! $show_current || get_query_var( 'cpage' ) ) {
            $cats = preg_replace( "#^(.+)$sep$#", "$1", $cats );
          }
          $cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#',
            $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2'
            . $link_in_after . '</a>' . $link_after, $cats );
          echo $cats;
          if ( get_query_var( 'cpage' ) ) {
            echo $sep . sprintf( $link, get_permalink(), get_the_title() )
                 . $sep . $before . sprintf( $text['cpage'],
                get_query_var( 'cpage' ) ) . $after;
          } else {
            if ( $show_current ) {
              echo $before . get_the_title() . $after;
            }
          }
        }
        // custom post type
      } elseif ( ! is_single() && ! is_page() && get_post_type() != 'post'
                 && ! is_404()
      ) {
        $post_type = get_post_type_object( get_post_type() );
        if ( get_query_var( 'paged' ) ) {
          echo $sep . sprintf( $link,
              get_post_type_archive_link( $post_type->name ),
              $post_type->label ) . $sep . $before . sprintf( $text['page'],
              get_query_var( 'paged' ) ) . $after;
        } else {
          if ( $show_current ) {
            echo $sep . $before . $post_type->label . $after;
          }
        }
      } elseif ( is_attachment() ) {
        if ( $show_home_link ) {
          echo $sep;
        }
        $parent = get_post( $parent_id );
        $cat    = get_the_category( $parent->ID );
        $cat    = $cat[0];
        if ( $cat ) {
          $cats = get_category_parents( $cat, true, $sep );
          $cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#',
            $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2'
            . $link_in_after . '</a>' . $link_after, $cats );
          echo $cats;
        }
        printf( $link, get_permalink( $parent ), $parent->post_title );
        if ( $show_current ) {
          echo $sep . $before . get_the_title() . $after;
        }
      } elseif ( is_page() && ! $parent_id ) {
        if ( $show_current ) {
          echo $sep . $before . get_the_title() . $after;
        }
      } elseif ( is_page() && $parent_id ) {
        if ( $show_home_link ) {
          echo $sep;
        }
        if ( $parent_id != $frontpage_id ) {
          $breadcrumbs = array();
          while ( $parent_id ) {
            $page = get_page( $parent_id );
            if ( $parent_id != $frontpage_id ) {
              $breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ),
                get_the_title( $page->ID ) );
            }
            $parent_id = $page->post_parent;
          }
          $breadcrumbs = array_reverse( $breadcrumbs );
          for ( $i = 0; $i < count( $breadcrumbs ); $i ++ ) {
            echo $breadcrumbs[ $i ];
            if ( $i != count( $breadcrumbs ) - 1 ) {
              echo $sep;
            }
          }
        }
        if ( $show_current ) {
          echo $sep . $before . get_the_title() . $after;
        }
      } elseif ( is_tag() ) {
        if ( get_query_var( 'paged' ) ) {
          $tag_id = get_queried_object_id();
          $tag    = get_tag( $tag_id );
          echo $sep . sprintf( $link, get_tag_link( $tag_id ), $tag->name )
               . $sep . $before . sprintf( $text['page'],
              get_query_var( 'paged' ) ) . $after;
        } else {
          if ( $show_current ) {
            echo $sep . $before . sprintf( $text['tag'],
                single_tag_title( '', false ) ) . $after;
          }
        }
      } elseif ( is_author() ) {
        global $author;
        $author = get_userdata( $author );
        if ( get_query_var( 'paged' ) ) {
          if ( $show_home_link ) {
            echo $sep;
          }
          echo sprintf( $link, get_author_posts_url( $author->ID ),
              $author->display_name ) . $sep . $before . sprintf( $text['page'],
              get_query_var( 'paged' ) ) . $after;
        } else {
          if ( $show_home_link && $show_current ) {
            echo $sep;
          }
          if ( $show_current ) {
            echo $before . sprintf( $text['author'], $author->display_name )
                 . $after;
          }
        }
      } elseif ( is_404() ) {
        if ( $show_home_link && $show_current ) {
          echo $sep;
        }
        if ( $show_current ) {
          echo $before . $text['404'] . $after;
        }
      } elseif ( has_post_format() && ! is_singular() ) {
        if ( $show_home_link ) {
          echo $sep;
        }
        echo get_post_format_string( get_post_format() );
      }
      echo $wrap_after;
    }
  } // end of dimox_breadcrumbs()
