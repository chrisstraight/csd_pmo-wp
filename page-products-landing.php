<header class="header-sub content-row">
  <div class="col-sm-12">
    <h1>Pre-engineered modular support and strut systems</h1>
    <nav class="breadcrumbs">
      <?php if ( function_exists( 'dimox_breadcrumbs' ) ) {
        dimox_breadcrumbs();
      } ?>
    </nav>
  </div>
</header>
<section>
  <div class="category-nav content-row">
    <div class="col-sm-6 category-nav-block">
      <div class="category-detail">
        <h2>Piping Supports</h2>
        <p>View dimensionally limited and load rated configurations</p>
      </div>
      <a href="<?php echo get_permalink( 29 ); ?>" class="nav-block-bg nav-block-bg-1">
        <div class="hover"></div>
        <div class="default"></div>
      </a>
    </div>
    <div class="col-sm-6 category-nav-block">
      <div class="category-detail">
        <h2>Cable Tray Supports</h2>
        <p>View dimensionally limited and load rated configurations</p>
      </div>
      <a href="<?php echo get_permalink( 28 ); ?>" class="nav-block-bg nav-block-bg-2">
        <div class="hover"></div>
        <div class="default"></div>
      </a>
    </div>
    <div class="col-sm-6 category-nav-block">
      <div class="category-detail">
        <h2>Platforms</h2>
        <p>View conceptual non-load rated configurations</p>
      </div>
      <a href="<?php echo get_permalink( 27 ); ?>" class="nav-block-bg nav-block-bg-3">
        <div class="hover"></div>
        <div class="default"></div>
      </a>
    </div>
    <div class="col-sm-6 category-nav-block">
      <div class="category-detail">
        <h2>Electrical and Instrumentation Stands</h2>
        <p>View conceptual non-load rated configurations</p>
      </div>
      <a href="<?php echo get_permalink( 10 ); ?>" class="nav-block-bg nav-block-bg-4">
        <div class="hover"></div>
        <div class="default"></div>
      </a>
    </div>
  </div>
</section>