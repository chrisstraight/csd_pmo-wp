<header class="header-sub content-row">
  <div class="col-sm-12">
    <h1>Put the Hilti Project Management Office to work for you</h1>
    <p>Fill out the form below with your project inquiry and a Hilti representative will contact you.
    </p>
  </div>
</header>

<div class="form-wrapper content-row-full">
<?php echo do_shortcode( '[contact-form-7 id="336" title="Contact form 1"]' ); ?>
