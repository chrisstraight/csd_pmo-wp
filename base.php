<?php

  use Roots\Sage\Setup;
  use Roots\Sage\Wrapper;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part( 'templates/head' ); ?>
  <body <?php body_class(); ?>>
    <div class="wrap" role="document">
      <div class="nav-main">
        <?php get_template_part( 'templates/nav-main' ); ?>
      </div>
      <main class="main">
        <?php include Wrapper\template_path(); ?>
      </main>
      <?php
        do_action( 'get_footer' );
        get_template_part( 'templates/footer' );
        wp_footer();
      ?>
    </div>
  </body>
</html>
